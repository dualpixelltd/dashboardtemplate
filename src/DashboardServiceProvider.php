<?php

namespace DualPixel\Dashboard;

use Illuminate\Support\ServiceProvider;

class DashboardServiceProvider extends ServiceProvider
{
    public function boot()
    {
      $this->loadViewsFrom(__DIR__.'/views', 'dashboard');
    }

    public function register()
    {

    }
}
